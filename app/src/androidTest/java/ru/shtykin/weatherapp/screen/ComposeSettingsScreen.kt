package ru.shtykin.weatherapp.screen

import androidx.compose.ui.semantics.SemanticsNode
import androidx.compose.ui.test.SemanticsMatcher
import androidx.compose.ui.test.SemanticsNodeInteractionsProvider
import io.github.kakaocup.compose.node.element.ComposeScreen
import io.github.kakaocup.compose.node.element.KNode
import io.github.kakaocup.compose.node.element.lazylist.KLazyListItemNode
import io.github.kakaocup.compose.node.element.lazylist.KLazyListNode
import ru.shtykin.weatherapp.extentions.LazyListItemPosition
import ru.shtykin.weatherapp.resources.UiTestTag

class ComposeSettingsScreen(semanticsProvider: SemanticsNodeInteractionsProvider) :
    ComposeScreen<ComposeSettingsScreen>(
        semanticsProvider = semanticsProvider,
        viewBuilderAction = { hasTestTag(UiTestTag.settings_screen_container) }
    )
{
    val cityTextField: KNode = child {
        hasTestTag(UiTestTag.settings_screen_city_text_field)
    }
    val addCityButton: KNode = child {
        hasTestTag(UiTestTag.settings_screen_add_city_button)
    }
    val backButton: KNode = child {
        hasTestTag(UiTestTag.settings_screen_back_button)
    }
    val listItem: KNode = child {
        hasTestTag(UiTestTag.settings_screen_list_item)
    }
    val itemCityText: KNode = child {
        hasTestTag(UiTestTag.settings_screen_item_city_text)
    }
    val list = KLazyListNode(
        semanticsProvider = semanticsProvider,
        viewBuilderAction = { hasTestTag(UiTestTag.settings_screen_list) },
        itemTypeBuilder = {
            itemType(::LazyListItemNode)
            itemType(::LazyListHeaderNode)
        },
        positionMatcher = { position -> SemanticsMatcher.expectValue(LazyListItemPosition, position) }

    )
}

class LazyListItemNode(
    semanticsNode: SemanticsNode,
    semanticsProvider: SemanticsNodeInteractionsProvider,
) : KLazyListItemNode<LazyListItemNode>(semanticsNode, semanticsProvider)

class LazyListHeaderNode(
    semanticsNode: SemanticsNode,
    semanticsProvider: SemanticsNodeInteractionsProvider,
) : KLazyListItemNode<LazyListHeaderNode>(semanticsNode, semanticsProvider) {
    val title: KNode = child {
        hasTestTag("LazyListHeaderTitle")
    }
}
