package ru.shtykin.weatherapp.screen

import androidx.compose.ui.test.SemanticsNodeInteractionsProvider
import io.github.kakaocup.compose.node.element.ComposeScreen
import io.github.kakaocup.compose.node.element.KNode
import ru.shtykin.weatherapp.resources.UiTestTag

class ComposeMainScreen(semanticsProvider: SemanticsNodeInteractionsProvider) :
    ComposeScreen<ComposeMainScreen>(
        semanticsProvider = semanticsProvider,
        viewBuilderAction = { hasTestTag(UiTestTag.main_screen_container) }
    )
{
    val addButton: KNode = child {
        hasTestTag(UiTestTag.main_screen_add_button)
    }
}