package ru.shtykin.weatherapp.test

import androidx.compose.ui.test.ExperimentalTestApi
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kaspersky.components.composesupport.config.withComposeSupport
import com.kaspersky.kaspresso.kaspresso.Kaspresso
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import io.github.kakaocup.compose.node.element.ComposeScreen.Companion.onComposeScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.shtykin.weatherapp.presentation.MainActivity
import ru.shtykin.weatherapp.screen.ComposeMainScreen
import ru.shtykin.weatherapp.screen.ComposeSettingsScreen
import ru.shtykin.weatherapp.screen.LazyListItemNode

@RunWith(AndroidJUnit4::class)
class ComposeTest : TestCase(
    kaspressoBuilder = Kaspresso.Builder.withComposeSupport()
) {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun testOpenSettings() = run {
        step("Open settings screen") {
            onComposeScreen<ComposeMainScreen>(composeTestRule) {
                addButton {
                    assertIsDisplayed()
                    performClick()
                }
            }
            onComposeScreen<ComposeSettingsScreen>(composeTestRule) {
                assertIsDisplayed()
            }
        }
    }

    @OptIn(ExperimentalTestApi::class)
    @Test
    fun testAddCity() = run {
        step("Open settings screen") {
            onComposeScreen<ComposeMainScreen>(composeTestRule) {
                addButton {
                    assertIsDisplayed()
                    performClick()
                }
            }
            onComposeScreen<ComposeSettingsScreen>(composeTestRule) {
                assertIsDisplayed()
            }
        }
        step("Add City") {
            onComposeScreen<ComposeSettingsScreen>(composeTestRule) {
                cityTextField {
                    assertIsDisplayed()
                    performTextInput("Париж")
                }
                addCityButton {
                    assertIsDisplayed()
                    performClick()
                }
                list{
                    firstChild<LazyListItemNode> {
                        assertIsDisplayed()
                    }
                }
            }
        }
    }
}
