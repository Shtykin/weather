package ru.shtykin.weatherapp.presentation.screen.settings

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ru.shtykin.weatherapp.extentions.lazyListItemPosition
import ru.shtykin.weatherapp.presentation.state.ScreenState
import ru.shtykin.weatherapp.resources.UiTestTag

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun SettingsScreen(
    uiState: ScreenState,
    onAddClick: ((String) -> Unit)?,
    onDeleteClick: ((String) -> Unit)?,
    onBackClick: (() -> Unit)?,
) {

    BackHandler {
        onBackClick?.invoke()
    }

    val focusManager = LocalFocusManager.current
    val cities = (uiState as? ScreenState.SettingsScreen)?.cities
    val keyboardController = LocalSoftwareKeyboardController.current
    var isFieldFocused by remember {
        mutableStateOf(false)
    }
    var searchText by remember { mutableStateOf(TextFieldValue(text = "")) }
    val searchChanged: (TextFieldValue) -> Unit = {
        searchText = it.copy(text = it.text)
    }
    val searchFocusRequester = FocusRequester()

    Scaffold(
        modifier = Modifier.semantics { testTag = UiTestTag.settings_screen_container },
        topBar = {
            TopAppBar(
                title = {
                    TextField(
                        value = searchText,
                        onValueChange = searchChanged,
                        modifier = Modifier
                            .fillMaxWidth()
                            .focusRequester(searchFocusRequester)
                            .onFocusChanged { isFieldFocused = it.isFocused }
                            .semantics { testTag = UiTestTag.settings_screen_city_text_field },
                        placeholder = { Text("Введите название города") },
                        label = { Text("Город") },
                        textStyle = TextStyle(fontSize = 16.sp),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                        keyboardActions = KeyboardActions(onDone = {
                            onAddClick?.invoke(searchText.text)
                            focusManager.clearFocus()
                            keyboardController?.hide()
                            searchText = TextFieldValue(text = "")
                        }),
                        colors = TextFieldDefaults.textFieldColors(
                            containerColor = MaterialTheme.colorScheme.background,
                            focusedIndicatorColor = Color.Transparent,
                            unfocusedIndicatorColor = Color.Transparent,
                            disabledIndicatorColor = Color.Transparent,
                            disabledLabelColor = Color.DarkGray,
                            focusedLabelColor = Color.DarkGray,
                            unfocusedLabelColor = Color.DarkGray
                        )
                    )
                },
                navigationIcon = {
                    IconButton(
                        modifier = Modifier.semantics {
                            testTag = UiTestTag.settings_screen_back_button
                        },
                        onClick = { onBackClick?.invoke() }
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Localized description"
                        )
                    }
                },
                actions = {
                    if (searchText.text.isNotEmpty()) {
                        IconButton(
                            modifier = Modifier.semantics {
                                testTag = UiTestTag.settings_screen_add_city_button
                            },
                            onClick = {
                                onAddClick?.invoke(searchText.text)
                                focusManager.clearFocus()
                                keyboardController?.hide()
                                searchText = TextFieldValue(text = "")
                            }) {
                            Icon(
                                imageVector = Icons.Default.Add,
                                contentDescription = "Localized description"
                            )
                        }
                    }
                }
            )
        }
    ) { innerPadding ->
        LazyColumn(
            modifier = Modifier
                .fillMaxHeight()
                .semantics { testTag = UiTestTag.settings_screen_list },
//                .testTag(UiTestTag.settings_screen_list),
            contentPadding = innerPadding
        ) {
            item {
                Divider(
                    thickness = 1.dp,
                    color = Color.Gray
                )
            }
            cities?.let {
                itemsIndexed(it) { index, city ->
                    CityCard(
                        modifier = Modifier.lazyListItemPosition(index).semantics { testTag = UiTestTag.settings_screen_list_item },
                        city = city,
                        onDeleteClick = onDeleteClick
                    )
                }
            }
        }

    }
}

@Composable
fun CityCard(
    modifier: Modifier,
    city: String,
    onDeleteClick: ((String) -> Unit)?,
) {
    Box(
        modifier = modifier.padding(8.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(Modifier.width(16.dp))
            Text(
                modifier = Modifier.semantics { testTag = UiTestTag.settings_screen_item_city_text },
                text = city,
                fontSize = 18.sp
            )
            Spacer(Modifier.weight(1f))
            IconButton(onClick = { onDeleteClick?.invoke(city) }) {
                Icon(
                    imageVector = Icons.Default.Delete,
                    contentDescription = "Localized description",
                    tint = Color.Black
                )
            }
            Spacer(Modifier.width(16.dp))
        }
    }
}