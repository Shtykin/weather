package ru.shtykin.weatherapp.resources

object UiTestTag {
    const val main_screen_container = "main_screen_container"
    const val main_screen_add_button = "login_screen_phone_text_field"

    const val settings_screen_container = "settings_screen_container"
    const val settings_screen_city_text_field = "settings_screen_city_text_field"
    const val settings_screen_add_city_button = "settings_screen_add_city_button"
    const val settings_screen_back_button = "settings_screen_back_button"
    const val settings_screen_list = "settings_screen_list"
    const val settings_screen_list_item = "settings_screen_item_of_list"
    const val settings_screen_item_city_text = "settings_screen_item_city_text"
}